﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CoffeeManagement.Controllers
{
    public class CustomerController : Controller
    {
        CoffeeContextDataContext db = new CoffeeContextDataContext();
        // GET: Coffee
       
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult RetrieveCustomer()
        {
            var customers = db.Customers.Select(cus => new
            {
                Id = cus.Id,
                Name = cus.Name,
                Email = cus.Email,
                Phone = cus.Phone,
                CustomerAddress = cus.CustomerAddress,
                Avatar = cus.Avatar
            });
           
            return Json(customers, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddCustomer([Bind(Exclude = "Id")] Customer customer)
        {
            db.Customers.InsertOnSubmit(customer);

            db.SubmitChanges();

            return Json(customer, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteCustomer(int Id)
        {
            var customer = db.Customers.ToList().Find(cus => cus.Id == Id);

            if (customer != null)
            {
                db.Customers.DeleteOnSubmit(customer);
                db.SubmitChanges();
            }

            return Json(customer, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult getCustomerDetail(int Id)
        {
            var customer = db.Customers.ToList().Find(cus => cus.Id == Id);

            return Json(customer, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult EditCustomer(Customer customer)
        {
            Customer cus = (from p in db.Customers
                            where p.Id == customer.Id
                            select p).SingleOrDefault();

            cus.Name = customer.Name;
            cus.Phone = customer.Phone;
            cus.Email = customer.Email;
            cus.CustomerAddress = customer.CustomerAddress;

            db.SubmitChanges();

            return Json(customer, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult searchCustomer(string textSearch)
        {
            var customers = db.Customers.ToList().Where(x => x.Name.Contains(textSearch)).ToList();

            return Json(customers, JsonRequestBehavior.AllowGet);
        }
    }
}