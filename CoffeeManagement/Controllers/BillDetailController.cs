﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CoffeeManagement.Controllers
{
    public class BillDetailController : Controller
    {
        CoffeeContextDataContext db = new CoffeeContextDataContext();

        // GET: BillDetail
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreatBillDetail([Bind(Exclude = "Id")] BillDetail billDetail)
        {
            db.BillDetails.InsertOnSubmit(billDetail);

            db.SubmitChanges();

            return Json(new { isSuccess = true }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RetrieveBillDetail()
        {
            var bills = db.Bills.Select(bill => new
            {
                billId = bill.Id,
                TableId = bill.TableId,
                TableName = bill.Table.Name,
                EmployeeId = bill.EmployeeId,
                EmployeeName = bill.Employee.Name,
                CustomerId = bill.CustomerId,
                CustomerName = bill.Customer.Name,
                Created = String.Format("{0:d/M/yyyy HH:mm:ss}", bill.Created),
                TotalPrice = bill.TotalPrice,
                BillStatus = bill.BillStatus,
                Note = bill.Note,
                Products = bill.BillDetails.Select(billDetail => new {
                    productId = billDetail.MenuId,
                    productName = billDetail.Menus.Name,
                    quantity = billDetail.Quantity,
                    price = billDetail.Price,
                    total = billDetail.Quantity * billDetail.Price
                })
            });

            return Json(bills, JsonRequestBehavior.AllowGet);
        }
    }
}