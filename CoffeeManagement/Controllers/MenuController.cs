﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CoffeeManagement.Controllers
{
    public class MenuController : Controller
    {
        CoffeeContextDataContext db = new CoffeeContextDataContext();

        // GET: Menu
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult RetrieveMenu()
        {
            var menus = db.Menus.Select(menu => new
            {
                Id = menu.Id,
                Name = menu.Name,
                Price = menu.Price,
                Image = menu.Image_path,
                CategoryId = menu.Category.Id,
                CategoryName = menu.Category.Name
            });

            return Json(menus, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddMenu([Bind(Exclude = "Id")] Menus menu)
        {
            db.Menus.InsertOnSubmit(menu);

            db.SubmitChanges();

            return Json(new { isSuccess = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteMenu(int Id)
        {
            var menu = db.Menus.ToList().Find(m => m.Id == Id);

            if (menu != null)
            {
                db.Menus.DeleteOnSubmit(menu);
                db.SubmitChanges();
            }

            return Json(new { isSuccess = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult getMenuDetail(int Id)
        {
            var menu = db.Menus.Where(x => x.Id == Id).Select(x => new
            {
                Id = x.Id,
                Name = x.Name,
                Price = x.Price,
                Image = x.Image_path,
                CategoryId = x.Category.Id,
                CategoryName = x.Category.Name
            }).SingleOrDefault();

            return Json(menu, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult EditMenu(Menus menu)
        {
            Menus m = (from p in db.Menus
                            where p.Id == menu.Id
                            select p).SingleOrDefault();

            m.Name = menu.Name;
            m.Price = menu.Price;
            m.CategoryId = menu.CategoryId;
            m.Image_path = menu.Image_path;

            db.SubmitChanges();

            return Json(new { isSuccess = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult searchMenu(string textSearch)
        {
            var menus = db.Menus.Where(x => x.Name.Contains(textSearch)).Select(x => new
            {
                Id = x.Id,
                Name = x.Name,
                Price = x.Price,
                Image = x.Image_path,
                CategoryId = x.Category.Id,
                CategoryName = x.Category.Name
            });            

            return Json(menus, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Upload()
        {
            string fileName = "";

            if (Request.Files.Count == 1)
            {
                var file = Request.Files[0];

                string rootPath = Server.MapPath("~/");

                fileName = System.IO.Path.GetFileName(file.FileName);
                string destFile = System.IO.Path.Combine(rootPath, "Assets\\images\\" + fileName);

                file.SaveAs(destFile);
            }

            return Json(new
            {
                Image_path = "../Assets/images/" + fileName
            }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult RetrieveMenuByCategoryId(int CategoryId)
        {
            var menus = db.Menus.Where(x => x.CategoryId == CategoryId).Select(x => new
            {
                Id = x.Id,
                Name = x.Name,
                Price = x.Price,
                Image_path = x.Image_path,
                CategoryId = x.CategoryId
            });

            return Json(menus, JsonRequestBehavior.AllowGet);
        }
    }
}