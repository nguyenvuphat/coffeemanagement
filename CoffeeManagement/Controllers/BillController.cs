﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CoffeeManagement.Controllers
{
    public class BillController : Controller
    {
        CoffeeContextDataContext db = new CoffeeContextDataContext();
        // GET: Bill
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CreateBill([Bind(Exclude = "Id")] Bill bill)
        {
            bill.Created = DateTime.Now;

            db.Bills.InsertOnSubmit(bill);

            db.SubmitChanges();

            return Json(new { BillId = bill.Id }, JsonRequestBehavior.AllowGet);
        }
    }

    
}