﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CoffeeManagement.Controllers
{
    public class AreasController : Controller
    {
        CoffeeContextDataContext db = new CoffeeContextDataContext();

        // GET: Areas
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult RetrieveArea()
        {
            var areas = db.Areas.Select(area => new
            {
                Id = area.Id,
                Name = area.Name
            });

            return Json(areas, JsonRequestBehavior.AllowGet);
        }

        
    }
}