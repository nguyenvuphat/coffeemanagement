﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CoffeeManagement.Controllers
{
    public class UserController : Controller
    {
        CoffeeContextDataContext db = new CoffeeContextDataContext();
        // GET: Coffee
        public ActionResult Login()
        {
            return View();
        }

        public ActionResult Register()
        {
            return View();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult RetrieveCustomer()
        {
            var customers = db.Customers.ToList();

            return Json(customers, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddCustomer([Bind(Exclude = "Id")] Customer customer)
        {
            db.Customers.InsertOnSubmit(customer);

            db.SubmitChanges();

            return Json(customer, JsonRequestBehavior.AllowGet);
        }
    }
}