﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CoffeeManagement.Controllers
{
    public class TableController : Controller
    {
        CoffeeContextDataContext db = new CoffeeContextDataContext();

        // GET: Table
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult RetrieveTable()
        {
            var tables = db.Tables.Select(table => new
            {
                Id = table.Id,
                Name = table.Name,
                TableStatus = table.TableStatus,
                AreaId = table.Area.Id,
                AreaName = table.Area.Name
            });

            return Json(tables, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult getTableDetail(int Id)
        {
            var table = db.Tables.Where(x => x.Id == Id).Select(x => new
            {
                Id = x.Id,
                Name = x.Name,
                TableStatus = x.TableStatus,
                AreaId = x.Area.Id,
                AreaName = x.Area.Name
            }).SingleOrDefault();

            return Json(table, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddTable([Bind(Exclude = "Id")] Table table)
        {
            db.Tables.InsertOnSubmit(table);

            db.SubmitChanges();

            return Json(new { isSuccess = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult EditTable(Table table)
        {
            Table t = (from p in db.Tables
                            where p.Id == table.Id
                            select p).SingleOrDefault();

            t.Name = table.Name;
            t.TableStatus = table.TableStatus;
            t.AreaID = table.AreaID;

            db.SubmitChanges();

            return Json(new { isSuccess = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteTable(int Id)
        {
            var table = db.Tables.ToList().Find(t => t.Id == Id);

            if (table != null)
            {
                db.Tables.DeleteOnSubmit(table);
                db.SubmitChanges();
            }

            return Json(new { isSuccess = true }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RetrieveTableByAreaId(int AreaId)
        {
            var tables = db.Tables.Where(x => x.AreaID == AreaId).Select(x => new
            {
                Id = x.Id,
                Name = x.Name,
                TableStatus = x.TableStatus,
                AreaId = x.Area.Id,
                AreaName = x.Area.Name
            });

            return Json(tables, JsonRequestBehavior.AllowGet);
        }
    }
}