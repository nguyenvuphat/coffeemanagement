﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CoffeeManagement.Controllers
{
    public class EmployeeController : Controller
    {
        CoffeeContextDataContext db = new CoffeeContextDataContext();

        // GET: Employee
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register([Bind(Exclude = "Id")] Employee employee)
        {
            db.Employees.InsertOnSubmit(employee);

            db.SubmitChanges();

            return Json(new { isSuccess = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Login(string Email, string Password)
        {
            var result = db.Employees.ToList().Find(employee => (employee.Email == Email && employee.Pass == Password));
            
            if (result != null)
            {
                Session["Id"] = result.Id;
                Session["email"] = result.Email;
                Session["name"] = result.Name;

                return Json(new { isSuccess = true }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { isSuccess = false }, JsonRequestBehavior.AllowGet);

        }
    }
}