﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CoffeeManagement.Controllers
{
    public class CategoryController : Controller
    {

        CoffeeContextDataContext db = new CoffeeContextDataContext();

        // GET: Category
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult RetrieveCategory()
        {
            var categories = db.Categories.Select(category => new
            {
                Id = category.Id,
                Name = category.Name
            });

            return Json(categories, JsonRequestBehavior.AllowGet);
        }
    }


}